import java.io.*;
import java.rmi.*;
import java.rmi.RemoteException;
import java.util.*;

public class Corrida implements Serializable {

  static final long serialVersionUID = 1L;
  private HashMap<Integer, Integer> grelha = new HashMap<Integer, Integer>();

  private int counter = 1;
  private int lap = 1;
  private ArrayList<String> clients = new ArrayList<String>();

  public Corrida() throws RemoteException {
    super();
  }

  HashMap<Integer, Integer> criarGrelha(ArrayList<Integer> cars) {
    int i;
    for (i = 0; i < cars.size(); i++) {

      int a = i + 1;
      grelha.put(a, cars.get(i));
    }

    return grelha;
  }

  ArrayList<Integer> getFirstSix() {

    ArrayList<Integer> firstSix = new ArrayList<Integer>();

    for (int i = 1; i < 7; i++) {

      firstSix.add(grelha.get(i));
    }

    return firstSix;
  }

  public static Object getKeyFromValue(Map hm, Object value) {
    for (Object o : hm.keySet()) {
      if (hm.get(o).equals(value)) {
        return o;
      }
    }
    return null;
  }

  void overtake(int overtaken, int overtaker) {
    Integer posOvertaken = (Integer) getKeyFromValue(grelha, overtaken);
    Integer posOvertaker = (Integer) getKeyFromValue(grelha, overtaker);

    if (posOvertaken > posOvertaker) {

    } else {

      ArrayList<Integer> cars = new ArrayList<>();
      for (int b = posOvertaken; b < posOvertaker; b++) {
        cars.add(grelha.get(b));
      }

      for (int a = 0; a < cars.size(); a++) {
        if (a == 0) {
          grelha.put(posOvertaken + 1, cars.get(a));
        } else {
          int b = posOvertaken + 1 + a;
          grelha.put(b, cars.get(a));
        }
      }
      grelha.put(posOvertaken, overtaker);
    }
  }

  HashMap<Integer, Integer> getGrelha() {

    return grelha;
  }

  int getLap() {
    return lap;
  }

  void setLap(int Lap) {
    lap = Lap;
  }

  void setCount(int Counter) {
    counter = Counter;
  }

  int getCounter() {

    return counter;
  }

  void increment() {

    counter = counter + 1;
  }

  ArrayList<String> getClients() {
    return clients;
  }

  void addClients(String client) {
    clients.add(client);
  }
}
