/* Inicia uma thread que fica à espera de ligação na porta 8081									*/
/* Quando aceita uma ligação, cria uma thread (byteServerHandler ) para atender essa ligação	*/

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.*;

public class byteServer {
  static int DEFAULT_PORT = 8082;

  static String SERVICE_NAME = "/gestorCorridaRemoto";

  static Corrida race = null;

  private static void bindRMI(ServeRace Serve) throws RemoteException {
    System.getProperties().put("java.security.policy", "./server.policy");

    if (System.getSecurityManager() == null) {
      System.setSecurityManager(new RMISecurityManager());
    }

    try {
      LocateRegistry.createRegistry(1099);
    } catch (RemoteException e) {

    }
    try {
      LocateRegistry.getRegistry("127.0.0.1", 1099).rebind(SERVICE_NAME, Serve);
    } catch (RemoteException e) {
      System.out.println("Registry not found");
    }
  }

  public static void main(String[] args) {

    int port = DEFAULT_PORT;
    // Cria um ServerSocket e associa-o a porta especificada na variavel port
    ServerSocket servidor = null;
    try {
      servidor = new ServerSocket(port);
    } catch (Exception e) {
      System.err.println("erro ao criar socket servidor...");
      e.printStackTrace();
      System.exit(-1);
    }
    ServeRace Serve = null;
    try {
      race = new Corrida();
      System.out.println("ByteCreated");
      Serve = new ServeRace();
    } catch (RemoteException e1) {
      System.err.println("unexpected error...");
      e1.printStackTrace();
    }

    try {
      bindRMI(Serve);
    } catch (RemoteException e1) {
      System.err.println("erro ao registar o stub...");
      e1.printStackTrace();
    }

    System.out.println("Servidor a' espera de ligacoes no porto " + port);

    int numberOfCars;
    Scanner sc = new Scanner(System.in); // Reading from System.in
    do {
      System.out.println("Enter the number of cars: ");
      while (!sc.hasNextInt()) {
        System.out.println("That's not a number!");
        sc.next(); // this is important!
      }
      numberOfCars = sc.nextInt();
      if (numberOfCars > 20 || numberOfCars < 6) {
        System.out.println("The number of cars has to be between 6 e 20");
      }
    } while (numberOfCars > 20 || numberOfCars < 6);

    int i;
    ArrayList<Integer> cars = new ArrayList<>();
    int car;
    for (i = 0; i < numberOfCars; i++) {

      int a = i + 1;
      do {
        System.out.println("Enter the number of the " + a + "º car");
        while (!sc.hasNextInt()) {
          System.out.println("That's not a number!");
          sc.next(); // this is important!
        }
        car = sc.nextInt();
        if (car > 100 || car < 0 || cars.contains(car)) {
          System.out.println("Esse carro já existe na grelha");
        }

      } while (car > 100 || car < 0 || cars.contains(car));

      cars.add(car);
    }

    sc.close();

    HashMap<Integer, Integer> grelha = race.criarGrelha(cars);

    System.out.println(grelha);
    for (Integer name : grelha.keySet()) {

      Integer key = name;
      Integer value = grelha.get(name);
      System.out.println("Car in " + key + "º position: " + value);
    }

    while (true) {
      try {
        // Aguarda que seja estabelecida alguma ligacao e quando isso acontecer cria um socket
        // chamado ligacao para atender essa ligação
        Socket ligacao = servidor.accept();
        byteServerHandler t = new byteServerHandler(ligacao, race);
        t.start();

      } catch (IOException e) {
        System.out.println("Erro na execucao do servidor: " + e);
        System.exit(1);
      }
    }
  }

  public static Corrida returnRace() {

    return race;
  }
}
