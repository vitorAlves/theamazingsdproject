import java.io.*;
import java.net.*;
import java.util.*;

public class byteServerHandler extends Thread {
  Socket ligacao;
  InputStream in;
  PrintWriter out;
  Corrida corrida;
  int overtaker;
  int overtaken;
  int lap;
  String name;
  int updates;

  public byteServerHandler(
      Corrida race,
      int n_monolugar_que_ultrapassou,
      int n_monolugar_que_foi_ultrapassado,
      int n_volta,
      String nomeParticipante,
      int n_atualizacoes) {
    this.corrida = race;
    this.overtaker = n_monolugar_que_ultrapassou;
    this.overtaken = n_monolugar_que_foi_ultrapassado;
    this.lap = n_volta;
    this.name = nomeParticipante;
    this.updates = n_atualizacoes;
  }

  public byteServerHandler(Socket ligacao, Corrida race) {

    this.corrida = race;
    this.ligacao = ligacao;

    try { // cria uma InputStream para ler os dados que chegam do socket
      this.in = ligacao.getInputStream();
      this.out = new PrintWriter(ligacao.getOutputStream());

    } catch (IOException e) {
      System.out.println("Erro na execucao do servidor: " + e);
      System.exit(1);
    }
  }

  public void run() {

    if (ligacao == null) {

      int currentCounter = corrida.getCounter();

      if (updates == 0 || updates <= currentCounter) {

      } else {

        corrida.setCount(updates);
        corrida.overtake(overtaken, overtaker);
        corrida.addClients(name);

        if (corrida.getLap() < lap) {

          corrida.setLap(lap);
        }

        System.out.println("Last client who updated: " + name);
        int clientslen = corrida.getClients().size();
        System.out.println("Clients who updated the system");
        for (int i = 0; i < clientslen; i++) {
          System.out.println(corrida.getClients().get(i));
        }
      }
    } else {
      try {

        byte data[] = new byte[100];

        byte overtakerB = (byte) in.read();
        int overtaker = new Byte(overtakerB).intValue();

        byte overtakenB = (byte) in.read();
        int overtaken = new Byte(overtakenB).intValue();

        byte lapB = (byte) in.read();
        int lap = new Byte(lapB).intValue();

        byte b = (byte) in.read();
        int length = new Byte(b).intValue();

        // le length bytes da stream para o array de bytes "data"
        length = in.read(data, 0, length);

        byte counter = (byte) in.read();
        int count = new Byte(counter).intValue();

        int counterU = corrida.getCounter();

        if (count == 0 || count <= counterU) {

          ArrayList<Integer> firstSix = corrida.getFirstSix();

          byte dataRes[] = new byte[6];

          Integer len = firstSix.size();
          for (int a = 0; a < firstSix.size(); a++) {
            Integer d = firstSix.get(a);
            byte c = d.byteValue();
            dataRes[a] = c;
          }

          byte response = len.byteValue();

          for (int a = 0; a < dataRes.length; a++) {

            out.println(dataRes[a]);
          }

          Integer lapCheck = corrida.getLap();
          byte bLapCheck = lapCheck.byteValue();
          out.println(bLapCheck);

          Integer cou = corrida.getCounter();
          byte bCount = cou.byteValue();
          out.println(bCount);

        } else {

          corrida.setCount(count);

          corrida.overtake(overtaken, overtaker);

          if (corrida.getLap() < lap) {

            corrida.setLap(lap);
          }

          // imprime em System.out os bytes do array "data" em binario e decimal (0-127)
          // transforma o array de bytes "data" numa string e imprime em System.out
          String msg = new String(data);

          // converte a string num array de bytes US-ASCII

          ArrayList<Integer> firstSix = corrida.getFirstSix();

          byte dataRes[] = new byte[6];

          Integer len = firstSix.size();
          for (int a = 0; a < firstSix.size(); a++) {

            Integer d = firstSix.get(a);
            byte c = d.byteValue();

            dataRes[a] = c;
          }

          byte response = len.byteValue();

          String client = new String(data);
          corrida.addClients(client);

          for (int a = 0; a < dataRes.length; a++) {
            out.println(dataRes[a]);
          }

          Integer lap2 = corrida.getLap();
          byte bLap2 = lap2.byteValue();
          out.println(bLap2);

          Integer coun = corrida.getCounter();
          byte bCount2 = coun.byteValue();
          out.println(bCount2);

          System.out.println("Estado da Corrida:");
          System.out.println(corrida.getGrelha().toString());

          System.out.println("Last client who updated: " + client);
          int clientslen = corrida.getClients().size();
          System.out.println("Clients who updated the system");
          for (int i = 0; i < clientslen; i++) {
            System.out.println(corrida.getClients().get(i));
          }
        }

        out.flush();
        in.close();

      } catch (IOException e) {
        System.out.println("Erro na execucao do servidor: " + e);
        System.exit(1);
      }
    }
  }
}
