import java.io.*;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.*;

public interface corridaRemotoInterface extends Remote {

  public Classificacao sincronizarClassificacao(
      int n_monolugar_que_ultrapassou,
      int n_monolugar_que_foi_ultrapassado,
      int n_volta,
      String nomeParticipante,
      int n_atualizacoes)
      throws RemoteException;
}
