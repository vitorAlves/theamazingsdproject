import java.io.*;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class ServeRace extends UnicastRemoteObject implements corridaRemotoInterface {

  Corrida a;
  Classificacao b;

  public ServeRace() throws RemoteException {
    super();
  }

  public Classificacao returnRace() {
    int i = 0;
    int lap = a.getLap();
    int counter = a.getCounter();
    ArrayList<Integer> first = a.getFirstSix();
    try {

      b = new Classificacao(first, lap, counter);
    } catch (IOException e) {
      System.out.println("Erro na criação da  classe: " + e);
      System.exit(1);
    }

    return b;
  }

  public Classificacao sincronizarClassificacao(
      int n_monolugar_que_ultrapassou,
      int n_monolugar_que_foi_ultrapassado,
      int n_volta,
      String nomeParticipante,
      int n_atualizacoes) {

    a = byteServer.returnRace();
    byteServerHandler handler =
        new byteServerHandler(
            a,
            n_monolugar_que_ultrapassou,
            n_monolugar_que_foi_ultrapassado,
            n_volta,
            nomeParticipante,
            n_atualizacoes);

    handler.start();

    try {
      TimeUnit.SECONDS.sleep(1);
    } catch (Exception e) {
    }

    return returnRace();
  };
}
