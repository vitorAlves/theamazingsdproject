import java.io.*;
import java.net.*;
import java.rmi.registry.LocateRegistry;
import java.util.*;

public class byteClient {

  static String SERVICE_NAME = "/gestorCorridaRemoto";

  public static void main(String[] args) {

    String servidor = "127.0.0.1";
    int porto = 8082;

    // Cria um objecto da c168lasse InetAddress com base no nome do servidor!
    InetAddress endereco = null;
    try {
      endereco = InetAddress.getByName(servidor);
    } catch (UnknownHostException e) {
      System.out.println("Endereco desconhecido: " + e);
      System.exit(-1);
    }

    // Cria um socket chamada ligacao e estabelece a ligacao indicada
    Socket ligacao = null;

    try {
      ligacao = new Socket(endereco, porto);
    } catch (Exception e) {
      System.err.println("erro ao criar socket...");
      e.printStackTrace();
      System.exit(-1);
    }
    try {

      BufferedReader in = new BufferedReader(new InputStreamReader(ligacao.getInputStream()));
      OutputStream out = ligacao.getOutputStream();
      Scanner scanner = new Scanner(System.in);
      Integer counter = 0;
      Integer overTaker = 0;
      Integer overTaken = 0;
      Integer lap = 0;
      String client = "";
      String rmi = "";

      do {
        System.out.print(
            "Insert 0 to get the last updated version of the grid \nInsert the last update number to update the grid \n");
        while (!scanner.hasNextInt()) {
          System.out.println("Please enter a valid number!");
          scanner.next(); // this is important!
        }
        counter = scanner.nextInt();
        if (counter < 0) {
          System.out.println("The number has to be 0 or other");
        }
      } while (counter < 0);

      byte bCounter = counter.byteValue();

      do {
        System.out.print("Send via RMI? (yes/no) \n");
        rmi = scanner.next();
        if (!rmi.equals("yes") && !rmi.equals("no")) {
          System.out.println(rmi.equals("yes"));
          System.out.println("Please enter Yes or No");
        }
      } while (!rmi.equals("yes") && !rmi.equals("no"));

      if (counter != 0) {

        do {
          System.out.print("Insert the number of the car who overtaked \n");
          while (!scanner.hasNextInt()) {
            System.out.println("Please enter a valid number!");
            scanner.next(); // this is important!
          }
          overTaker = scanner.nextInt();
          if (overTaker < 0) {
            System.out.println("Please enter a valid number!");
          }
        } while (overTaker < 0);
        byte bOverTaker = overTaker.byteValue();

        do {
          System.out.print("Insert the number of the car who was overtaken \n");
          while (!scanner.hasNextInt()) {
            System.out.println("Please enter a valid number!");
            scanner.next(); // this is important!
          }
          overTaken = scanner.nextInt();
          if (overTaken < 0) {
            System.out.println("Please enter a valid number!");
          }
        } while (overTaken < 0);
        byte bOverTaken = overTaken.byteValue();

        do {
          System.out.print("Insert the number of the lap \n");
          while (!scanner.hasNextInt()) {
            System.out.println("Please enter a valid number!");
            scanner.next(); // this is important!
          }
          lap = scanner.nextInt();
          if (lap < 0) {
            System.out.println("Please enter a valid number!");
          }
        } while (lap < 0);

        byte bLap = lap.byteValue();

        System.out.print("Insert the name of the client who is doing the update \n");
        client = scanner.next();
        Integer lengthClient = client.length();

        byte bClient = lengthClient.byteValue();

        int totalSize = lengthClient.byteValue() + 4;
        byte total[] = new byte[totalSize];

        byte dataClient[] = client.getBytes("US-ASCII");
      }

      if (rmi.equals("yes")) {

        Classificacao first = null;
        try {

          corridaRemotoInterface stub =
              (corridaRemotoInterface) LocateRegistry.getRegistry("127.0.0.1").lookup(SERVICE_NAME);

          first = stub.sincronizarClassificacao(overTaker, overTaken, lap, client, counter);

          System.out.println("---------------------Race Status---------------------");
          String[] places = {"First", "Second", "Third", "Fourth", "Fifth", "Sixth"};
          for (int i = 0; i < 6; i++) {

            System.out.println(places[i] + " place : " + first.getFirst().get(i));
          }

          System.out.println("Lap nº: " + first.getLap());

          System.out.println("Update nº: " + first.getCounter());

          // System.out.println(first.getGrelha());
          // System.out.println(first.getLap());

          // System.out.println(first.getFirstSix());

          ligacao.close();
        } catch (Exception e) {
          System.err.println("Error");
          e.printStackTrace();
        }

      } else {
        byte bOverTaker = overTaker.byteValue();
        byte bOverTaken = overTaken.byteValue();
        byte bLap = lap.byteValue();
        Integer lengthClient = client.length();
        byte bClient = lengthClient.byteValue();
        int totalSize = lengthClient.byteValue() + 4;
        byte total[] = new byte[totalSize];
        byte dataClient[] = client.getBytes("US-ASCII");

        out.write(bOverTaker);
        out.write(bOverTaken);
        out.write(bLap);
        out.write(bClient);
        out.write(dataClient);
        out.write(bCounter);

        out.flush();

        System.out.println("---------------------Race Status---------------------");
        String[] places = {"First", "Second", "Third", "Fourth", "Fifth", "Sixth"};
        for (int i = 0; i < 6; i++) {

          String pos = in.readLine();
          Byte posB = Byte.parseByte(pos);
          Integer posI = posB.intValue();
          int a = i + 1;
          System.out.println(places[i] + " place : " + posI);
        }
        System.out.println("Lap nº: " + in.readLine());

        System.out.println("Update nº: " + in.readLine());

        // termina a ligacao
        ligacao.close();
      }
    } catch (IOException e) {
      System.out.println("Erro ao comunicar com o servidor: " + e);
      System.exit(1);
    }
  }
}
