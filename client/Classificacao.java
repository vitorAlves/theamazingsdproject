import java.io.*;
import java.rmi.*;
import java.rmi.RemoteException;
import java.util.*;

public class Classificacao implements Serializable {

  static final long serialVersionUID = 1L;

  private int counter = 1;
  private int lap = 1;
  private ArrayList<Integer> first = new ArrayList<Integer>();

  public Classificacao(ArrayList<Integer> first, int lapO, int update) throws RemoteException {
    super();
    this.lap = lapO;
    this.first = first;
    this.counter = update;
  }

  int getLap() {
    return lap;
  }

  void setLap(int Lap) {
    lap = Lap;
  }

  void setCount(int Counter) {
    counter = Counter;
  }

  int getCounter() {

    return counter;
  }

  ArrayList<Integer> getFirst() {
    return first;
  }
}
